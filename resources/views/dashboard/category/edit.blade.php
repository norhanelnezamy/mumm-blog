@extends('dashboard.layout.app')
@section('content')
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Update Category Data</h4>
        <div class="d-flex align-items-center">
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ url('dashboard/category/') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Update Category Data</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title"></h4>
            <h5 class="card-subtitle"></h5>
            <form class="form" method="post" action="{{ url('dashboard/category/'.$category->id) }}" >
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="form-group row">
                <label for="example-text-input" class="col-2 col-form-label">Category Name</label>
                <div class="col-10">
                  <input name="name" class="form-control" type="text" value="{{ old('name', $category->name) }}">
                  @if ($errors->has('name'))
                    <p style="color:red">{{ $errors->first('name') }}</p>
                  @endif
                </div>
              </div>
              <div class="form-actions float-right">
                <a href="{{ url('dashboard/category') }}" type="button" class="btn btn-dark">Cancel</a>
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
