@extends('dashboard.layout.app')
@section('content')
  @php
  $categories = getCategories();
  @endphp
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">Insert New post</h4>
        <div class="d-flex align-items-center">
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ url('dashboard/post') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Insert New post</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title"></h4>
            <h5 class="card-subtitle"></h5>
            <form class="form" method="post" action="{{ url('dashboard/post') }}">
              {{ csrf_field() }}
              <div class="form-group row">
                <label for="example-text-input" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                  <input name="title" class="form-control" type="text" value="{{ old('title') }}">
                  @if ($errors->has('title'))
                    <p style="color:red">{{ $errors->first('title') }}</p>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label for="example-month-input2" class="col-2 col-form-label">category)</label>
                <div class="col-10">
                  <select class="custom-select category-select col-12" id="example-month-input2" name="category_id">
                    <option selected="">Choose category Name</option>
                    @foreach ($categories as $key => $category)
                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('category_id'))
                    <p style="color:red">{{ $errors->first('category_id') }}</p>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label for="example-text-input" class="col-2 col-form-label">Description</label>
                <div class="col-10">
                  <textarea name="describe" class="form-control" type="text">{{ old('describe') }}</textarea>
                  @if ($errors->has('describe'))
                    <p style="color:red">{{ $errors->first('describe') }}</p>
                  @endif
                </div>
              </div>
              <div class="form-actions float-right">
                <a href="{{ url('dashboard/post') }}" type="button" class="btn btn-dark">Cancel</a>
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
  <script>
  @if (old('category_id'))
  $('.category-select option[value="{{ old('category_id') }}"]').prop('selected', true);
  @endif
  </script>
@endsection
