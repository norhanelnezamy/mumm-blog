@extends('dashboard.layout.app')
@section('content')
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-5 align-self-center">
        <h4 class="page-title">posts</h4>
        <div class="d-flex align-items-center">
        </div>
      </div>
      <div class="col-7 align-self-center">
        <div class="d-flex no-block justify-content-end align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ url('dashboard/post') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">posts</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <!-- File export -->
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="file_export" class="table table-striped table-bordered display">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>category Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $count=1;
                  @endphp
                  @foreach ($posts as $key => $post)
                    <tr>
                      <td>{{ $count }}</td>
                      <td>{{ $post->title }}</td>
                      <td>{{ $post->category->name   }}</td>
                      <td>{{ $post->describe }}</td>
                      <td>
                        <a href="{{ url('dashboard/post/'.$post->id.'/edit') }}" class="btn btn-sm btn-icon btn-pure btn-outline" data-toggle="tooltip" data-original-title="Update"><i class="ti-pencil-alt" aria-hidden="true"></i></a>
                        <a href="#" class="warning-alert btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete" data-url="{{ url('dashboard/post/'.$post->id) }}" data-method="DELETE" data-msg="Are you sure ?" data-csrf="{{csrf_token()}}"><i class="ti-close" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    @php
                      $count++
                    @endphp
                  @endforeach
                </tbody>
              </table>
              {{ $posts->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
