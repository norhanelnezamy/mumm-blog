@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="">
              <!-- Title -->
              <h1 class="mt-4">Title: {{ $post->title }}</h1>
              <p class="lead">
                Category Name :
                <a href="#">{{ $post->category->name }}</a>
              </p>
              <hr>
              <!-- Date/Time -->
              <p>
                Posted on {{ Carbon\Carbon::parse($post->created_at)->format('D, d M Y H:i') }}
              </p>
              <hr>
              <!-- Post Content -->
              <p class="lead">
                {{ $post->describe }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
