@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Welcome To Home Page</div>

          <div class="panel-body">
            @foreach ($posts as $key => $post)
              <div class="card mb-4">
                <div class="card-body">
                  <h2 class="card-title">{{ $post->title }}</h2>
                  <p class="card-text">
                    {{ $post->describe }}
                  </p>
                  <a href="{{ url('post/'.$post->id) }}">Read More →</a>
                </div>
                <div class="card-footer text-muted">
                  Posted on {{ Carbon\Carbon::parse($post->created_at)->format('D, d M Y H:i') }}
                </div>
              </div>
            @endforeach
            {{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
