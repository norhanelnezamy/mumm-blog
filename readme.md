# Mumm Assignment

## Installation and configuration steps

```bash
git clone https://gitlab.com/norhanelnezamy/mumm-blog.git
```
```bash
composer update
```
```bash
php artisan key:generate
```
please create a database in your SQL serve

```bash
cp -i .env.example .env
```
configure your database connection

```bash
php artisan migrate
```
```bash
php artisan db:seed
```
```bash
php artisan serve
```

## Usage

Use [landing page](http://127.0.0.1:8000) to get user interface.

Use this credintial to login as a dashboard admin:

email: admin@admin.com

password: password
