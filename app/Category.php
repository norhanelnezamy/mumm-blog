<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name'
  ];

  /**
  * insert new category row.
  */
  public static function insertRow($request)
  {
    $category = new Category();
    $category->name = $request->name;
    $category->save();
    return $category;
  }

  /**
  * update category row.
  */
  public static function updateRow($request, $id)
  {
    $category = Category::findOrFail($id);
    $category->name = $request->name;
    $category->save();
    return $category;
  }

  /**
  * Get the posts that owns the category.
  */
  public function posts()
  {
    return $this->hasMany('App\Post');
  }
}
