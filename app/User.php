<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name', 'email', 'password', 'type'
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
      'password', 'remember_token',
    ];

    /**
    * insert new user row.
    */
    public static function insertRow($request, $type)
    {
      $user = new User();
      $user->email = $request->email;
      $user->name = $request->name;
      $user->password = bcrypt($request->password);
      $user->type = $type;
      $user->save();
      return $user;
    }

    /**
    * update user row.
    */
    public static function updateRow($request, $id)
    {
      $user = User::findOrFail($id);
      $user->name = $request->name;
      $user->email = $request->email;
      if (!empty($request->password)) {
        $user->password = bcrypt($request->password);
      }
      $user->save();
      return $user;
    }

    /**
    * Get the posts that owns the user.
    */
    public function posts()
    {
      return $this->hasMany('App\Post');
    }
}
