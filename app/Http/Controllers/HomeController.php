<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class HomeController extends Controller
{

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $data['posts'] = Post::with('category')->paginate(10);
    return view('home', $data);
  }

  /**
  * get posts by category
  *
  * @return \Illuminate\Http\Response
  */
  public function getPostsByCategory($id)
  {
    $data['posts'] = Post::where('category_id', $id)->with('category')->paginate(10);
    return view('home', $data);
  }

  /**
  * Show the single post details.
  *
  * @return \Illuminate\Http\Response
  */
  public function showPost($id)
  {
    $data['post'] = Post::where('id', $id)->with('category')->first();
    return view('single-post', $data);
  }
}
