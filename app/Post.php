<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'title', 'describe', 'category_id'
  ];

  /**
  * insert new post row.
  */
  public static function insertRow($request)
  {
    $post = new Post();
    $post->title = $request->title;
    $post->describe = $request->describe;
    $post->category_id = $request->category_id;
    $post->save();
    return $post;
  }

  /**
  * update post row.
  */
  public static function updateRow($request, $id)
  {
    $post = Post::findOrFail($id);
    $post->title = $request->title;
    $post->describe = $request->describe;
    $post->category_id = $request->category_id;
    $post->save();
    return $post;
  }

  /**
  * Get the posts that owns the post.
  */
  public function category()
  {
    return $this->belongsTo('App\Category');
  }
}
