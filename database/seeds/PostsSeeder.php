
<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $posts = [];
    for ($i=1; $i <=  15 ; $i++) {
      $post = [
        'title' => "Post $i Title",
        'describe' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?

        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.

        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!

        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.",
        'category_id' => rand(1, 5),
      ];
      array_push($posts, $post);
    }
    Post::insert($posts);
  }
}
