
<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $categories = [];
    for ($i=1; $i <=  5 ; $i++) {
      $categories[] = [
        'id' => $i,
        'name' => "category $i",
      ];
    }

    Category::insert($categories);
  }
}
